const AWS = require("aws-sdk");
var AWSCognito = require('amazon-cognito-identity-js');
AWS.config.update({
    region: 'ap-south-1',
    accessKeyId: 'AKIAI44BEJILJVI552IQ',
    secretAccessKey: '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'
});
cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider();

var userPoolParams = {
    PoolName: 'preferredUserPool',
    AdminCreateUserConfig: {
        AllowAdminCreateUserOnly: false
    },
    AutoVerifiedAttributes: [
        'email'
    ],
    Schema: [
        {
            AttributeDataType: 'String',
            DeveloperOnlyAttribute: false,
            Mutable: true,
            Name: 'title',
            Required: false,
            StringAttributeConstraints: {
                MaxLength: '100',
                MinLength: '1'
            }
        },
        {
            AttributeDataType: 'String',
            DeveloperOnlyAttribute: false,
            Mutable: true,
            Name: 'company',
            Required: false,
            StringAttributeConstraints: {
                MaxLength: '100',
                MinLength: '1'
            }
        }
    ],
    AliasAttributes: ['preferred_username']
};

 function createAppClient(orgIdentifier, appClientName) {
    const newAppClientParams = {
        ClientName: appClientName,
        UserPoolId: orgIdentifier,
        GenerateSecret: false,
        ReadAttributes: [
            'given_name',
            'family_name',
            'nickname',
            'custom:title',
            'custom:company',
            'email',
            'preferred_username'
        ],
        WriteAttributes: [
            'given_name',
            'family_name',
            'nickname',
            'custom:title',
            'custom:company',
            'email',
            'preferred_username'
        ]
    };
    return newAppClientParams;
};

cognitoIdentityServiceProvider.createUserPool(userPoolParams, function (err, userPoolData) {
    if (err) {
        console.log('UserPool Creation Error ::: ' + JSON.stringify(err));
    } else {
        console.log('UserPool Creation Successful ::: ' + JSON.stringify(userPoolData));
        var appClientParams = createAppClient(userPoolData.UserPool.Id, 'preferredAppClient');
        cognitoIdentityServiceProvider.createUserPoolClient(appClientParams, function (err, appClientData) {
            if (err) {
                console.log('AppClient Creation Error ::: ' + JSON.stringify(err));
            } else {
                console.log('AppClient Creation Successful ::: ' + JSON.stringify(appClientData));
            }
        });
    }
});
var loginUser = require("loginRequest");

exports.handler = (event, context, callback) => {
    var jsonEventObj = event;
    if (event.body) {
        jsonEventObj = JSON.parse(event.body);
    }

    loginUser(jsonEventObj.userName, jsonEventObj.password)
        .then((result) => {
            var resultFinal = {
                statusCode: 200,
                body: JSON.stringify(result),
                headers: {
                    "Access-Control-Allow-Origin": "*", // Required for CORS support to work
                    "Access-Control-Allow-Credentials": true // Required for cookies, authorization headers with HTTPS
                }

            }

            console.log("Request executed: \n" + JSON.stringify(result));
            console.log("Request executed resultFinal: \n" + JSON.stringify(resultFinal));

            callback(null, resultFinal);
        })
        .catch(error => {
            console.log('Inside error block :::::')
            console.log(JSON.stringify(error));

            var resultError = {
                statusCode: 401,
                body: JSON.stringify(error),
                headers: {
                    "Access-Control-Allow-Origin": "*", // Required for CORS support to work
                    "Access-Control-Allow-Credentials": true // Required for cookies, authorization headers with HTTPS
                }

            };

            console.log("Request failed: \n" + JSON.stringify(error));
            console.log("Request failed resultError: \n" + JSON.stringify(resultError));
            callback(null, resultError);
        });
};

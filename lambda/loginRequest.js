var AWS = require('aws-sdk');
var AWSCognito = require('amazon-cognito-identity-js');

var loginUser = function(userName, password) {
    return new Promise((resolve, reject) => {
        if (process.env && process.env.USERPOOL_NAME) {
            var orgDetails = getUserPoolDetails(process.env.USERPOOL_NAME);
            orgDetails.then((result) => {
                try {
                    var authenticationData = {
                        Username: userName,
                        Password: password,
                    };
                    var authenticationDetails = new AWSCognito.AuthenticationDetails(authenticationData);
                    var poolData = {

                        UserPoolId: result.Id,
                        ClientId: result.clientId
                    };

                    var userPool = new AWSCognito.CognitoUserPool(poolData);

                    var userData = {
                        Username: userName,
                        Pool: userPool
                    };
                    var cognitoUser = new AWSCognito.CognitoUser(userData);
                    cognitoUser.authenticateUser(authenticationDetails, {
                        onSuccess: function(result) {
                            console.log("Cognito authenticationDetails successs : " + JSON.stringify(result));
                            resolve(result);
                        },
                        onFailure: function(err) {
                            console.log("Cognito authenticationDetails error : " + JSON.stringify(err));
                            var errorResp = err.code;
                            reject(errorResp);
                        },
                        newPasswordRequired: function(userAttributes, requiredAttributes) {
                            cognitoUser.completeNewPasswordChallenge(password, null, this);
                        }
                    });
                }
                catch (error) {
                    console.log("Error: \n" + JSON.stringify(error));
                    var errorResp = 'Error while authentication';

                    reject(errorResp);
                }
            }).catch((error) => {
                console.log("Userpool and AppClient details could not be located for the given poolName: " + process.env.USERPOOL_NAME);
                var errorResp = 'Error locating UserPool';
                reject(errorResp);
            });
        }
        else {
            reject("Lambda Environment Variable is not set");
        }

    });
};

var getUserPoolDetails = function(userpool) {
    var userPoolData = {};
    return new Promise((resolve, reject) => {
        const params = {
            // todo : For now just fetching 60, need to update the logic to fetch and iterate over all the pools.
            MaxResults: 60
        };

        var cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider();
        cognitoIdentityServiceProvider.listUserPools(params, function(err, data) {
            if (err) {
                console.log("Error listing userpools: " + JSON.stringify(err));
                reject(err);
            }
            else {
                if (!data || data == null || !data.UserPools || data.UserPools.length === 0) {
                    console.log("Error listing userpools");
                    reject("Userpools not found");
                }
                data.UserPools.forEach(function(pool) {
                    var userPoolName = pool.Name;
                    if (userPoolName === userpool) {
                        console.log("UserPoolFound: \n" + JSON.stringify(pool));
                        userPoolData.Id = pool.Id;
                        const params = {
                            MaxResults: 1,
                            UserPoolId: userPoolData.Id
                        };
                        cognitoIdentityServiceProvider.listUserPoolClients(params, function(err, data) {
                            if (!err) {
                                if (!data || data == null || !data.UserPoolClients || data.UserPoolClients.length == 0) {
                                    console.log("Appclient could not be located.");
                                    reject();
                                }
                                console.log("AppClients located for the userpool: " + JSON.stringify(data));
                                userPoolData.clientId = data.UserPoolClients[0].ClientId;
                                resolve(userPoolData);
                            }
                            else {
                                console.log("AppClient could not be located for userpool \n" + JSON.stringify(err));
                                reject();
                            }
                        });
                    }
                });
            }
        });
    });
};

module.exports = loginUser;
